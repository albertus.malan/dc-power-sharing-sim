# DC Power Sharing Simulation

This repo contains the simulation code for the paper 'Passivity-Based Power Sharing and Voltage Regulation in DC Microgrids with Unactuated Buses' available at https://doi.org/10.1109/TCST.2024.3372308

Running the simulation requires at least Matlab 2020b along with Simulink and the Simscape Electrical Toolbox.

To run the simulation, load the workspaces 'workspace_Leaky.mat' or 'workspace_Ideal.mat' for the simulation with leaky or with ideal integrators, respectively. Thereafter, open 'DC_Network.slx' in Simulink and run the simulation.